package it.uniba.pathway;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

    public class Database extends AsyncTask<String, Void, String> {
        private static final String url = "jdbc:mysql://progetto-mobile-2021.mysql.database.azure.com:3306/sms?useSSL=true&requireSSL=false";
        private static final String user = "AdminDB@progetto-mobile-2021";
        private static final String pass = "esameSMS2021";

        public static final String FLAG_INSERT = "i";
        public static final String FLAG_SELECT = "s";
        public static final String FLAG_DELETE = "d";


        public interface AsyncResponse {
            void processFinish(String output);
        }

        public AsyncResponse delegate = null;

        public Database(AsyncResponse delegate){
            this.delegate = delegate;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Statement st=null;
            ResultSet rs = null;
            String result = "";

            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = DriverManager.getConnection(url, user, pass);
                System.out.println("Databaseection success");

                st = con.createStatement();

            }catch(Exception e){
                result = "error connection";
            }

            if(params[0].equals(FLAG_SELECT)){
                try {
                    rs = st.executeQuery(params[1]);
                    ResultSetMetaData rsmd = rs.getMetaData();

                    while (rs.next()) {
                        for (int i=1; i<=Integer.parseInt(params[2]); i++) {
                            result += rs.getString(i).toString()+",";
                        }
                        result+="\n";
                    }

                    } catch (Exception e) {
                        result="query Error -> "+params[1]+" "+e;
                    }
                }

            if(params[0].equals(FLAG_INSERT)){

                try{
                    st.executeUpdate(params[1]);
                    result="true";
                }catch (Exception e){
                    result="query Error -> "+params[1]+" "+e;
                }


            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            System.out.println("PROVA-> "+result);
            delegate.processFinish(result);
        }
    }
