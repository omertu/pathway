package it.uniba.pathway;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Homepage extends AppCompatActivity{

    private FloatingActionButton fabMain;
    private FloatingActionButton fabEdit;
    private FloatingActionButton fabAdd;
    private FloatingActionButton fabDelete;

    private AlertDialog.Builder dialogBulider;
    private AlertDialog dialog;
    private EditText zoneName;
    private Button confirm,annulla;
    private static String ID_MUSEO="1";
    Map<String,String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        map=new HashMap<String,String>();
        ArrayList<Button> zones=new ArrayList<Button>();

        fabMain = (FloatingActionButton)  findViewById(R.id.floatingActionButton4);
        fabEdit = (FloatingActionButton)  findViewById(R.id.floatingActionButton5);
        fabAdd = (FloatingActionButton)  findViewById(R.id.floatingActionButton6);
        fabDelete = (FloatingActionButton) findViewById(R.id.floatingActionButton2);


        Database asyncTask = (Database) new Database(new Database.AsyncResponse() {

            @Override
            public void processFinish(String output) {

                System.out.println(output);
                String[] zone = output.split("\n");

                for (String infoZona:zone) {
                    String[] iz = infoZona.split(",");
                    map.put(iz[0],iz[1]);
                }

            }
        }).execute(Database.FLAG_SELECT, "SELECT * FROM zone where idMuseo="+ID_MUSEO,"3");


        /*GridView gv = findViewById(R.id.gridview);
        for (Map.Entry<String,String> set:map.entrySet()) {
                Button b = new Button(this);
                b.setText(set.getValue());
                b.setId(Integer.parseInt(set.getValue()));

                zones.add(b);
        }*/

        fabMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Homepage.this, "Animation", Toast.LENGTH_SHORT).show();
            }
        });

        //Edit zone -> goTo EditActivity

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewDialg();
            }
        });


        // Add zone -> goTo AddActivity
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Homepage.this, "Modifica", Toast.LENGTH_SHORT).show();
         }
        });

        // Delete zone -> goTo DeleteActivity
        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Homepage.this, "Delete", Toast.LENGTH_SHORT).show();
            }
        });



    }


public void createNewDialg(){
        dialogBulider = new AlertDialog.Builder(this);
        final View popupView = getLayoutInflater().inflate(R.layout.popup,null);
        zoneName = (EditText) popupView.findViewById(R.id.editTextTextPersonName);
        confirm = (Button) popupView.findViewById(R.id.button2);
        annulla = (Button) popupView.findViewById(R.id.button3);



        dialogBulider.setView(popupView);
        dialog=dialogBulider.create();
        dialog.show();


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insert nel Db
                String name = zoneName.getText().toString();

                if(!name.isEmpty()){
                    //insert DB


                    Database asyncTask = (Database) new Database(new Database.AsyncResponse() {

                        @Override
                        public void processFinish(String output) {

                            boolean result=Boolean.parseBoolean(output);

                            if(result){
                                Toast.makeText(Homepage.this, "Zona: "+ name+" inserita con successo", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }else{
                                Toast.makeText(Homepage.this, "Errore durante l'inserimento: riprovare più tardi", Toast.LENGTH_SHORT).show();
                            }


                        }
                    }).execute(Database.FLAG_INSERT, "insert into zone(Nome,idMuseo) values (\"" + name + "\","+ID_MUSEO+");");


                }else{
                    Toast.makeText(Homepage.this, "Inserire un nome valido", Toast.LENGTH_SHORT).show();
                }

            }
        });

        annulla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

}